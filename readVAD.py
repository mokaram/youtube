#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "Lianlian Qi"
__copyright__ = "Copyright 2018 Gweek"
__credits__ = ["Lianlian Qi, Gweek"]
__license__ = "Gweek Ltd"
__version__ = "1.0"
__maintainer__ = "Lianlian Qi"
__email__ = "lianlian@gweekspeech.com"
__status__ = "Production"
# ===========================
# ==============================================
# Description:
# This Python script convert



# ======================================================
# ======================================================================================
def readSHoUTfile(shoutFile, jobPath, threshold):
    vad = { "tokens": [] }
    for line in open(shoutFile).readlines():
        if line.strip().startswith("SPEAKER"):
            segLine = line.strip().split()

            start_time = float(segLine[3])
            end_time = float(segLine[3])+float(segLine[4])
            duration = float(segLine[4])

            if segLine[7].upper() != "SPEECH":
                isSil = True
            else:
                isSil = False

            if isSil and duration > threshold:
                isSG = True
            else:
                isSG = False

            vad["tokens"].append({'isSIL': isSil, 'isSG': isSG, 'start_time': start_time, 'end_time': end_time, 'duration': duration})


    # Writing VAD info file including VAD-based SG detection.
    with open(jobPath + "/vad.json", 'w') as outfile:  # Saving VAD data.
        json.dump(vad, outfile, indent=4)


    return vad

# ======================================================================================

if __name__ == '__main__':
    import argparse, json, os

    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('jobPath', help='Path to the job folder.')
    parser.add_argument('sampleRate', help='Audio file Sample Rate.')

    args = parser.parse_args()
    jobPath = args.jobPath
    sampleRate = args.sampleRate

    vadDict = readSHoUTfile(jobPath + "/vad" + "/vad.shout", jobPath , 0.55)





