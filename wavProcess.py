#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "Lianlian Qi"
__copyright__ = "Copyright 2018 Gweek"
__credits__ = ["Lianlian Qi, Gweek"]
__license__ = "Gweek Ltd"
__version__ = "1.0"
__maintainer__ = "Lianlian Qi"
__email__ = "lianlian@gweekspeech.com"
__status__ = "Production"
# ===========================
# ==============================================
# Description:
# This Python script implements choping the speech audio into 1~2 mins long
#
# Dependencies
# sudo pip install json
# sudo pip install wave

# To run the client:
# audiopath and output path needed
# ======================================================
import wave
import json
# ======================================================
def raedWav(audioPath):
    wavFile = wave.open(audioPath, 'r')
    frames = wavFile.readframes(wavFile.getnframes())

    return [frames, wavFile.getnchannels(), wavFile.getsampwidth(), wavFile.getframerate(), wavFile.getnframes()]
# ======================================================
def writeWav(frames, audioPath, nchannels, framerate, sampwidth):
    outWavFile = wave.open(audioPath, 'w')
    outWavFile.setnchannels(nchannels)
    outWavFile.setframerate(framerate)
    outWavFile.setnframes(len(frames)/sampwidth)
    outWavFile.setsampwidth(sampwidth)

    outWavFile.writeframesraw(frames)
    outWavFile.close()
# ======================================================
def getSegment(frames, startTime, endTime, framerate, sampwidth):
    starTime = framerate * sampwidth * startTime
    endTime = framerate * sampwidth * endTime

    if starTime%1 < .5:
        starTime = int(starTime)
    else:
        starTime = int(starTime)+1

    if endTime % 1 < .5:
        endTime = int(endTime)
    else:
        endTime = int(endTime) + 1

    return frames[starTime:endTime]
# ======================================================
def readVAD(audiopath):
    with open (audiopath[:-9] + "/vad.json",'r') as jsonfile:
        files = json.load(jsonfile)
        end_list = []
        start_list = [0.0]
        audio_end = files["tokens"][-1]["end_time"]
        for i in files["tokens"]:
            # print i
            if i['isSIL'] and i['duration'] >= 1.0:
                endtime = i['start_time'] + 0.2
                starttime = i['end_time']-0.2
                end_list.append(endtime)
                start_list.append(starttime)
        end_list.append(audio_end)

    return [start_list, end_list]
# ======================================================


audiopath = "/Users/qilianlian/Desktop/youtube_audio/SpaceX/audio.wav"
out = "/Users/qilianlian/Desktop/youtube_audio/SpaceX/segments/"


[frames, nchannels, sampwidth, framerate, nframes] = raedWav(audiopath)


[start_list, end_list] = readVAD(audiopath)
print start_list
print end_list
# print readVAD(audiopath)

segFrames = ''

duration = 0
c = 1
n = 0
for i in range(len(start_list)):
    if duration <= 60.00:
        duration += end_list[i]-start_list[i]
        segFrames += getSegment(frames, start_list[i], end_list[i], framerate, sampwidth)
        while duration > 60.00 and duration <= 120.00:
            print duration
            print i
            seglen = int(duration)* sampwidth * framerate
            print seglen
            # writeWav(segFrames[n:n+seglen], out + str(c) + ".wav", nchannels, framerate, sampwidth)
            n = n + seglen
            print n
            print "------------------------------"
            c = c + 1
            duration = 0



