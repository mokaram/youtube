from __future__ import unicode_literals
import youtube_dl
import os

def downloadyoutube(youtubeURL, outFolderPath):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': outFolderPath + '%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'wav',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([youtubeURL])
        audiofolder = ydl_opts['outtmpl'].split("%")[0]
        # audiofolder = ydl_opts['outtmpl']
        print audiofolder

        for filename in os.listdir(audiofolder):
            if filename != ".DS_Store":
                # filename = filename.replace(" ", "_")
                if filename[-4:] == ".wav":
                    filenames = filename[:-4].replace(" ", "_")

                    os.mkdir(audiofolder + filenames)
                    os.rename(audiofolder + "/" + filename, audiofolder + "/" + filenames + "/sound.wav")
                    os.system("sox  " + audiofolder + "/" + filenames + "/sound.wav" + "  -r 16k -b 16 -c 1  " + audiofolder + "/" + filenames + "/audio.wav" )

# ======================================================================================
# if __name__ == '__main__':
#     import argparse
#
#     parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
#
#     parser.add_argument('youtubeLink', help='Path to the youtube link.')
#     parser.add_argument('outputFolder', help='Path to the output folder')
#
#     args = parser.parse_args()
#
#     youtubeLink = args.youtubeLink
#     outputFolder = args.outputFolder
#
youtubeLink = "https://www.youtube.com/watch?v=ft8sxHf-9M8"
outputFolder = "/home/lian/Desktop/urgent"


downloadyoutube(youtubeLink,outputFolder)