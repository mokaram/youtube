# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for automatically download videos from Youtube and extract audios from it.

After converted to audio files, we can cut,concatenate any part of audios by exact time or speech activity information.

### Contribution guidelines ###

To cut and concatenate the youtube audios:

step1: provide the link on youtube and run download.py

step2: run
'./vad/release/src/shout_segment -a ' + audioPath + ' -ams vad/models/shout.sad -l msg001 -mo ' + outFolderPath + '/vad.shout >> ' + outFolderPath + '/vadlog.txt')

to get vad.shout file

step3: read vad.shout and convert it to vad.json

run: readVad.py

step4: chop and concatenate speech part
run wavProcess.py

step5: in order to get json file and datapoints, we need to send audios to dev server to see the results

then we could see the score or generate Elan files for speech analysis to do annotation tasks.