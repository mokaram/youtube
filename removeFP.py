import json
from pprint import pprint

import wave
import os
# ======================================================
class wavProcess():
    def __init__(self, audioPath):
        self.audioPath = audioPath
        self.raedWav(self.audioPath)
    # ======================================================
    def raedWav(self, audioPath):
        wavFile = wave.open(audioPath, 'r')
        self.nchannels = wavFile.getnchannels()
        self.sampwidth = wavFile.getsampwidth()
        self.framerate = wavFile.getframerate()
        self.nframes = wavFile.getnframes()
        self.frames = wavFile.readframes(self.nframes)
        self.waveLength = float(self.nframes) / float(self.framerate)
    # ======================================================
    def writeWav(self, frames, outPath):
        outWavFile = wave.open(outPath, 'w')
        outWavFile.setnchannels(self.nchannels)
        outWavFile.setframerate(self.framerate)
        outWavFile.setnframes(len(frames)/self.sampwidth)
        outWavFile.setsampwidth(self.sampwidth)
        outWavFile.writeframesraw(frames)
        outWavFile.close()
    # ======================================================
    def getSegment(self, startTime, endTime):
        starTime = int(round(self.framerate * self.sampwidth * startTime))
        endTime = int(round(self.framerate * self.sampwidth * endTime))
        return self.frames[starTime:endTime]
    # ======================================================
    def writeSegment(self, startTime, endTime, outPath):
        self.writeWav(self.getSegment(startTime, endTime), outPath)
    # ======================================================

si1Path = "/home/lian/Desktop/audio/removeFP/mixed2/si1.json"
audioPath = "/home/lian/Desktop/audio/removeFP/mixed2/original.wav"

import json,pprint
startTime = []
endTime = []
with open(si1Path) as jsonfile:
    jsonFile = json.load(jsonfile)
    tokens = jsonFile['tokens']
    pprint.pprint(tokens)
    for i in range(len(tokens)):
        if tokens[i]["isFP"]:
            cut_startTime = tokens[i]['start_time']
            cut_endTime = tokens[i]['end_time']

            startTime.append(cut_startTime)
            endTime.append(cut_endTime)


for item in range(len(startTime)):
    start = startTime[item]
    end = endTime[item]
    audio = wavProcess(audioPath)
    output = "/home/lian/Desktop/audio/removeFP/mixed2/fpseg" + str(item) + ".wav"
    audio.writeSegment(start, end, output)

