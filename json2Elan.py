# ==============================================
# Description:
# This Python script convert the json file to ELAN format
# To run the script:
# python jsonToElan.py
# ==============================================
# ======================================================================================
# Write textgrid file
def writefile(jobPath):
    textgridfile = open(jobPath + "/si1.textgrid", "w")
    # ======================================================================================
    # Write ELAN header
    textgridfile.write('File type = "ooTextFile"\n')
    textgridfile.write('Object class = "TextGrid"\n')
    textgridfile.write('\n')
    textgridfile.write('xmin = 0.0\n')
    textgridfile.write("xmax =" + str(data['tokens'][-1]["end_time"]) + "\n")
    textgridfile.write('tiers? <exists>\n')
    textgridfile.write('size = 2 \n')
    textgridfile.write('item []:\n')
    textgridfile.write('\n')

    # ======================================================================================
    # Write first tier
    textgridfile.write('\t\t' + 'item['+str(1)+"]:" + '\n')
    textgridfile.write('\t\t\t\t' + 'class = "IntervalTier"\n')
    textgridfile.write('\t\t\t\t' + 'name = "automatic"\n')
    textgridfile.write('\t\t\t\t' + 'xmin = 0.0\n')
    textgridfile.write('\t\t\t\t' + "xmax =" + str(data['tokens'][-1]["end_time"]) + "\n")
    textgridfile.write('\t\t\t\t' + 'intervals: size =' + str(len(data['tokens'])) + '\n')


    seglist = []
    n = 1
    segap = []
    for i in range(len(data['tokens'])):
        data['tokens'][i]["text"] = data['tokens'][i]["text"].lower()
        if data['tokens'][i]['isFP']:
            data['tokens'][i]["text"] = data['tokens'][i]["text"] + "_FP"
        if data['tokens'][i]['isDM']:
            data['tokens'][i]["text"] = data['tokens'][i]["text"] + "_DM"
        if data['tokens'][i]['isDR']:
            data['tokens'][i]["text"] = data['tokens'][i]["text"] + "_DR"
        if data['tokens'][i]['isSW']:
            data['tokens'][i]["text"] = data['tokens'][i]["text"] + "_SW"

        seglist.append(data['tokens'][i]["text"])

        if data['tokens'][i]['isSG']:
            segap.append(data['tokens'][i]["text"])
            text = " ".join(seglist).replace("sil","")
            length = len(seglist)
            startime = data['tokens'][i-length+1]["start_time"]
            endtime = data['tokens'][i-1]["end_time"]
            seglist = []
            textgridfile.write('\t\t\t\t' + 'intervals [' + str(n) + ']:' + '\n')
            textgridfile.write('\t\t\t\t\t\t' +'xmin = ' + str(startime) + '\n')
            textgridfile.write('\t\t\t\t\t\t' + 'xmax = ' + str(endtime) + '\n')
            textgridfile.write('\t\t\t\t\t\t' + 'text = ' + str(text) + '\n')
            n = n +1

    token = []
    sil = []
    last_sen = []
    for i in range(len(data['tokens'])):
        token.append(data['tokens'][i]['text'])
    for i, value in enumerate(token):
        # print i,value
        if data['tokens'][i]["isSG"]:
            sil.append(i)

    for n in range(sil[-1] + 1, len(data['tokens'])):
        last_sen.append(data['tokens'][n]['text'])
        sen = " ".join(last_sen).replace("sil","")

    textgridfile.write('\t\t\t\t' + 'intervals [' + str(len(segap)+1) + ']:' + '\n')
    textgridfile.write('\t\t\t\t\t\t' +'xmin = ' + str(data['tokens'][sil[-1] + 1]['start_time']) + '\n')
    textgridfile.write('\t\t\t\t\t\t' + "xmax =" + str(data['tokens'][-1]["end_time"]) + "\n")
    textgridfile.write('\t\t\t\t\t\t' + 'text = ' + str(sen) + '\n')

    # ======================================================================================
    # Write second tier
    textgridfile.write('\t\t' + 'item['+str(2)+"]:" + '\n')
    textgridfile.write('\t\t\t\t' + 'class = "IntervalTier"\n')
    textgridfile.write('\t\t\t\t' + 'name = "manual-annotation"\n')
    textgridfile.write('\t\t\t\t' + 'xmin = 0.0\n')
    textgridfile.write('\t\t\t\t' + "xmax =" + str(data['tokens'][-1]["end_time"]) + "\n")
    textgridfile.write('\t\t\t\t' + 'intervals: size =' + str(len(data['tokens'])) + '\n')
    seglist = []
    n = 1
    segap = []
    for i in range(len(data['tokens'])):
        seglist.append(data['tokens'][i]["text"])
        if data['tokens'][i]['isSG']:
            segap.append(data['tokens'][i]["text"])
            # print seglist
            text = " ".join(seglist).replace("sil","")
            length = len(seglist)
            startime = data['tokens'][i-length+1]["start_time"]
            endtime = data['tokens'][i-1]["end_time"]
            seglist = []
            textgridfile.write('\t\t\t\t' + 'intervals [' + str(n) + ']:' + '\n')
            textgridfile.write('\t\t\t\t\t\t' +'xmin = ' + str(startime) + '\n')
            textgridfile.write('\t\t\t\t\t\t' + 'xmax = ' + str(endtime) + '\n')
            textgridfile.write('\t\t\t\t\t\t' + 'text = ' + str(text) + '\n')
            n = n +1

    token = []
    sil = []
    last_sen = []
    for i in range(len(data['tokens'])):
        token.append(data['tokens'][i]['text'])
    for i, value in enumerate(token):
        # print i,value
        if data['tokens'][i]["isSG"]:
            sil.append(i)

    for n in range(sil[-1] + 1, len(data['tokens'])):
        last_sen.append(data['tokens'][n]['text'])
        sen = " ".join(last_sen).replace("sil","")

    textgridfile.write('\t\t\t\t' + 'intervals [' + str(len(segap)+1) + ']:' + '\n')
    textgridfile.write('\t\t\t\t\t\t' +'xmin = ' + str(data['tokens'][sil[-1] + 1]['start_time']) + '\n')
    textgridfile.write('\t\t\t\t\t\t' + "xmax =" + str(data['tokens'][-1]["end_time"]) + "\n")
    textgridfile.write('\t\t\t\t\t\t' + 'text = ' + str(sen) + '\n')

    # ======================================================================================
    # Close the file

    textgridfile.close()

# ======================================================================================
if __name__ == '__main__':
    import argparse, json,os
    from pprint import pprint

    # parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    #
    # parser.add_argument('jobPath', help='Path to the json file folder')


    # args = parser.parse_args()
    # jobPath = args.jobPath

    # ======================================================================================
    # Read Json si1.json file
    # for i in os.listdir(jobPath):
    #     if i != ".DS_Store":
    #         foldname = i.split('.')[0]
    #         for jsonfile in os.listdir(jobPath + "/" + foldname):
    #             if jsonfile.endswith('.json'):
    #                 with open(jobPath + "/" + foldname + "/" + jsonfile) as jFile:
    #                     data = json.load(jFile)

    jobfolder = "/home/lian/Desktop/dataset100_highSW/"
    for folder in os.listdir(jobfolder):

        with open(jobfolder + folder + "/si1.json", "r") as jsonfile:
            data = json.load(jsonfile)



        # pprint(data)
        writefile(jobfolder + folder)