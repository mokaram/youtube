import wave
import os
# ======================================================
class wavProcess():
    def __init__(self, audioPath):
        self.audioPath = audioPath
        self.raedWav(self.audioPath)
    # ======================================================
    def raedWav(self, audioPath):
        wavFile = wave.open(audioPath, 'r')
        self.nchannels = wavFile.getnchannels()
        self.sampwidth = wavFile.getsampwidth()
        self.framerate = wavFile.getframerate()
        self.nframes = wavFile.getnframes()
        self.frames = wavFile.readframes(self.nframes)
        self.waveLength = float(self.nframes) / float(self.framerate)
    # ======================================================
    def writeWav(self, frames, outPath):
        outWavFile = wave.open(outPath, 'w')
        outWavFile.setnchannels(self.nchannels)
        outWavFile.setframerate(self.framerate)
        outWavFile.setnframes(len(frames)/self.sampwidth)
        outWavFile.setsampwidth(self.sampwidth)
        outWavFile.writeframesraw(frames)
        outWavFile.close()
    # ======================================================
    def getSegment(self, startTime, endTime):
        starTime = int(round(self.framerate * self.sampwidth * startTime))
        endTime = int(round(self.framerate * self.sampwidth * endTime))
        return self.frames[starTime:endTime]
    # ======================================================
    def writeSegment(self, startTime, endTime, outPath):
        self.writeWav(self.getSegment(startTime, endTime), outPath)
    # ======================================================



audioPath = "/Users/qilianlian/Desktop/youtube_audio/Humanoid_Robot_Tells_Jokes_on_GMB_Good_Morning_Britain/audio.wav"
output = "/Users/qilianlian/Desktop/youtube_audio/Humanoid_Robot_Tells_Jokes_on_GMB_Good_Morning_Britain/segments/6.wav"
audio = wavProcess(audioPath)
print audio.audioPath
audio.writeSegment(122.0,129.0,output)

